var request = require('request');

var geocodeAddress = (address) => {
  return new Promise((resolve, reject) => {
    request({
      url: `https://maps.googleapis.com/maps/api/geocode/json?address=${address}`,
      json: true
    }, (error, resp, body) => {
      if(error) {
        reject('Unable to connect to Google servers');
      } else if(body.status === 'ZERO_RESULTS') {
        reject('Unable to find tha address');
      } else if(body.status === 'OK') {
        resolve({
          address:body.results[0].formatted_address,
          lat: body.results[0].geometry.location.lat,
          lng: body.results[0].geometry.location.lng
        })
      } else {
        reject('Something went wrong');
      }
    })
  });
};

geocodeAddress('95050')
.then(console.log)
.catch(console.error)