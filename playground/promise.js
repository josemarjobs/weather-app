var asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' && typeof b === 'number') {
        resolve(a+b)
      } else {
        reject('Arguments must be number');
      }
    }, 1500)
  });
}

asyncAdd(2, 3)
.then(r => {
  console.log('Result: ', r)
  return asyncAdd(r, 33)
})
.then(console.log)
.catch(console.error)

var somePromise = new Promise ((resolve, reject) => {
  setTimeout(() => {
    reject('Hey, It failed!');
  }, 1000)
  // resolve('Hey, It worked!');
})

somePromise
  .then(console.log)
  .catch(console.error)