var getUser = (id, callback) => {
  var user = {
    id,
    name: "Peter Griffin"
  }
  setTimeout(() => {
    callback(user)
  }, 1000)
};

getUser(1, (user) => {
  console.log(user);
});