const request = require('request');

const geocodeAddress = (address, cb) => {
  const encodedAddress = encodeURIComponent(address);
  request({
    url: `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`,
    json: true
  }, (error, resp, body) => {
    if(error) {
      cb('Unable to connect to Google servers');
    } else if(body.status === 'ZERO_RESULTS') {
      cb('Unable to find tha address');
    } else if(body.status === 'OK') {
      cb(null, {
        address:body.results[0].formatted_address,
        lat: body.results[0].geometry.location.lat,
        lng: body.results[0].geometry.location.lng
      })
    } else {
      cb('Something went wrong');
    }
  })
}

module.exports = {
  geocodeAddress
}