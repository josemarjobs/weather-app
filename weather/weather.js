const request = require('request');

const getWeather = (lat, lng, cb) => {
  request({
    url: `https://api.darksky.net/forecast/7e0f0a56d82538e5293a49537fc1ce51/${lat},${lng}`,
    json: true
  }, (error, resp, body) => {
    if (!error && resp.statusCode === 200) {
      cb(null, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature,
        icon: body.currently.icon,
      });
    } else {
      cbg('Unable to fetch weather.');
    }
  })
}

module.exports = {
  getWeather
}