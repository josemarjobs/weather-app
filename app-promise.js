const yargs = require('yargs');
const axios = require('axios');

const argv = yargs
  .options({
    address: {
      demand: true,
      alias: 'a',
      describe: 'Address to fetch weather for',
      string: true
    }
  })
  .help()
  .alias('help', 'h')
  .argv;

const encodedAddress = encodeURIComponent(argv.address);
const geocodeUrl = `https://maps.googleapis.com/maps/api/geocode/json?address=${encodedAddress}`;

axios.get(geocodeUrl).then(response => {
  if (response.data.status === 'ZERO_RESULTS') {
    throw new Error('Unable to find the address.')
  }
  var lat = response.data.results[0].geometry.location.lat
  var lng = response.data.results[0].geometry.location.lng
  var weatherUrl = `https://api.darksky.net/forecast/7e0f0a56d82538e5293a49537fc1ce51/${lat},${lng}`

  console.log(response.data.results[0].formatted_address);
  return axios.get(weatherUrl)
})
.then(response => {
  var temp = response.data.currently.temperature;
  var apparentTemp = response.data.currently.apparentTemperature;
  console.log(`It is ${temp}ºF, but it feels like ${apparentTemp}ºF`)
})
.catch(e => {
  if(e.code === 'ENOTFOUND') {
    console.log('Unable to connect to API Server.')
  } else {
    console.log(e.message)
  }
})
  